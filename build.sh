#!/usr/bin/env bash

set -e

function echo_divider {
    echo "************************************"
}

function run_tests {
    echo_divider
    echo "Running tests"
    echo_divider
    python runtests.py
}

function upload_pypi {
    echo_divider
    echo "Uploading to pypi..."
    echo_divider

    FILE="$HOME/.pypirc"
    if [ -f "$FILE" ]; then
        echo "$FILE found."
    else 
        cp ".pypirc" "$FILE"
        echo "$FILE created."
    fi

    python -m twine upload dist/*
}

function update_version {
    echo_divider
    echo "Updating PYPI version...... NO IMPLEMENTADO"
    echo_divider
    #python update_version.py
}

function build {
    echo_divider
    echo "Building..."
    echo_divider
    rm -rf dist/ build/ django_cfdi.egg-info/
    python setup.py bdist_wheel
}

run_tests
update_version
build
upload_pypi
