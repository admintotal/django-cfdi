# Generated by Django 3.2.8 on 2022-01-03 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cfdi', '0015_cfdi_motivo_cancelacion_cfdi'),
    ]

    operations = [
        migrations.AddField(
            model_name='cfdi',
            name='cfdis_relacionados',
            field=models.ManyToManyField(related_name='documentos_relacion', to='cfdi.Cfdi'),
        ),
    ]
