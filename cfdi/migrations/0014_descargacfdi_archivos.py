# Generated by Django 2.2.4 on 2021-07-26 23:41

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cfdi', '0013_auto_20210726_2306'),
    ]

    operations = [
        migrations.AddField(
            model_name='descargacfdi',
            name='archivos',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=255), default=list, size=None),
        ),
    ]
