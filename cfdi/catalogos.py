MOTIVOS_TRASLADO = (
	("01", "01 Envío de mercancias facturadas con anterioridad"),
	("02", "02 Reubicación de mercancías propias"),
	("03", "03 Envío de mercancías objeto de contrato de consignación"),
	("04", "04 Envío de mercancías para posterior enajenación"),
	("05", "05 Envío de mercancías propiedad de terceros"),
	("99", "99 Otros"),
)

TIPOS_RELACION = (
    ("01", "01 Nota de crédito de los documentos relacionados"),
    ("02", "02 Nota de débito de los documentos relacionados"),
    ("03", "03 Devolución de mercancía sobre facturas o traslados previos"),
    ("04", "04 Sustitución de los CFDI previos"),
    ("05", "05 Traslados de mercancias facturados previamente"),
    ("06", "06 Factura generada por los traslados previos"),
    ("07", "07 CFDI por aplicación de anticipo"),
    ("08", "08 Factura generada por pagos en parcialidades"),
    ("09", "09 Factura generada por pagos diferidos"),
)