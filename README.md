# django-cfdi
django-cfdi is a tool for friendly work with CFDI's.


## Installation
```
pip install django-cfdi
```
Crear un virtualenv e instalar requirements_dev.txt


## Activar validación de CFDI's
Para poder utilizar la función `cfdi.utils.validar_cfdi` es necesario especificar las siguientes variables en su archivo `settings.py` y pueden variar los valores dependiendo de su instalación del proyecto validacfdi-cli (https://gitlab.com/admintotal/validacfdi-cli)
```
CFDI_NODE_BIN = "/home/root/.nvm/versions/node/v12.14.1/bin/node"
CFDI_VALIDADOR_PATH = "/srv/Projects/validacfdi-cli/index.js"
```



## Para subir a PYPI:
Conseguir el archivo ```.pypirc``` (buscarlo en zulip>code)
```
python build.py
```