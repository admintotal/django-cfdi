#!/usr/bin/env python
from setuptools import setup, find_packages

import cfdi

setup(
    name="django-cfdi",
    version=cfdi.__version__,
    description=cfdi.__doc__,
    packages=find_packages(),
    url="https://gitlab.com/admintotal/django-cfdi",
    author="admintotal-dev",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    include_package_data=True,
    install_requires=[
        'django>=1.8;python_version>="3"',
        'cfdiclient>=1.4.1;python_version>="3"',
        # 'suds;python_version>="3"',
        # 'lxml;python_version>="3"',
        # 'requests;python_version>="3"',
    ],
)
