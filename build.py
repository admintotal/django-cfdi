import subprocess, sys

def get_version_actaul():
	for l in open("cfdi/__init__.py", "r").readlines():
		if "__version__" in l:
			return l.replace(" ", "").replace("__version__=", "").replace('"', "")

def get_nueva_version(anterior):
    split_version = anterior.split(".")
    n = int(split_version.pop()) + 1
    split_version.append(n)
    return ".".join([str(x) for x in split_version])

def set_version(version):
	txt = ""
	for l in open("cfdi/__init__.py", "r").readlines():
		if "__version__" in l:
			txt += f"__version__ = \"{version}\"\n"
		else:
			txt += l

	open("cfdi/__init__.py", "w").write(txt)

version_actual = get_version_actaul()
nueva_version = get_nueva_version(version_actual)
set_version(nueva_version)

cambios_version = "Comentarios generados de manera automática build.py"
try:
	commit_message = sys.argv[1]
except:
	commit_message = "Nueva versión generada por build.py"


print("")
print("Actualizando versión en el repositorio...")
open("cfdi/version", "w").write(nueva_version)

print("Creando nuevo tag en el repositorio....")
subprocess.call(["git", "pull"])
subprocess.call(["git", "add", "."])
subprocess.call(["git", "commit", "-am", commit_message])
subprocess.call(["git", "push"])
subprocess.call(["./build.sh"])

print("*" * 25)
print(f"Versión anterior: {version_actual}")
print(f"Nueva version: {nueva_version}")
print("*" * 25)
